import React from 'react';
import {View,Text,StyleSheet,ScrollView,Image,TouchableOpacity} from 'react-native';

const sofi = (props) => {
    return(
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onpress}>
                <Image source={require('../assets/like.png')} style={styles.edit}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/komentar.png')} style={styles.edit}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/bagikan.png')} style={styles.edit}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source={require('../assets/comen.jpg')} style={{width:32,height:30,marginLeft:200,marginTop:10}}/>
            </TouchableOpacity>
        </ScrollView>
    );
};

const styles=StyleSheet.create({
    edit : {
        width:32,
        height:30,
        marginLeft:10,
        marginTop:10
    }
});
export default sofi;